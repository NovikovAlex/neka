$(document).ready(function () {
    $(".btn-down-view").on("click", "a", function () {
        var id = $(this).attr('href'),
            top = $(id).offset().top - 0;
        $('body,html').animate({scrollTop: top}, 1000);
        return false;
    });
});

/* Tree()
 * ======
 * Converts a nested list into a multilevel
 * tree view menu.
 *
 * @Usage: $('.my-menu').tree(options)
 *         or add [data-widget="tree"] to the ul element
 *         Pass any option as data-option="value"
 */
+function ($) {
    'use strict';

    var DataKey = 'lte.tree';

    var Default = {
        animationSpeed: 500,
        accordion     : true,
        followLink    : false,
        trigger       : '.treeview a'
    };

    var Selector = {
        tree        : '.tree',
        treeview    : '.treeview',
        treeviewMenu: '.treeview-menu',
        open        : '.menu-open, .active',
        li          : 'li',
        data        : '[data-widget="tree"]',
        active      : '.active'
    };

    var ClassName = {
        open: 'menu-open',
        tree: 'tree'
    };

    var Event = {
        collapsed: 'collapsed.tree',
        expanded : 'expanded.tree'
    };

    // Tree Class Definition
    // =====================
    var Tree = function (element, options) {
        this.element = element;
        this.options = options;

        $(this.element).addClass(ClassName.tree);

        $(Selector.treeview + Selector.active, this.element).addClass(ClassName.open);

        this._setUpListeners();
    };

    Tree.prototype.toggle = function (link, event) {
        var treeviewMenu = link.next(Selector.treeviewMenu);
        var parentLi     = link.parent();
        var isOpen       = parentLi.hasClass(ClassName.open);

        if (!parentLi.is(Selector.treeview)) {
            return;
        }

        if (!this.options.followLink || link.attr('href') === '#') {
            event.preventDefault();
        }

        if (isOpen) {
            this.collapse(treeviewMenu, parentLi);
        } else {
            this.expand(treeviewMenu, parentLi);
        }
    };

    Tree.prototype.expand = function (tree, parent) {
        var expandedEvent = $.Event(Event.expanded);

        if (this.options.accordion) {
            var openMenuLi = parent.siblings(Selector.open);
            var openTree   = openMenuLi.children(Selector.treeviewMenu);
            this.collapse(openTree, openMenuLi);
        }

        parent.addClass(ClassName.open);
        tree.slideDown(this.options.animationSpeed, function () {
            $(this.element).trigger(expandedEvent);
        }.bind(this));
    };

    Tree.prototype.collapse = function (tree, parentLi) {
        var collapsedEvent = $.Event(Event.collapsed);

        //tree.find(Selector.open).removeClass(ClassName.open);
        parentLi.removeClass(ClassName.open);
        tree.slideUp(this.options.animationSpeed, function () {
            //tree.find(Selector.open + ' > ' + Selector.treeview).slideUp();
            $(this.element).trigger(collapsedEvent);
        }.bind(this));
    };

    // Private

    Tree.prototype._setUpListeners = function () {
        var that = this;

        $(this.element).on('click', this.options.trigger, function (event) {
            that.toggle($(this), event);
        });
    };

    // Plugin Definition
    // =================
    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var data  = $this.data(DataKey);

            if (!data) {
                var options = $.extend({}, Default, $this.data(), typeof option == 'object' && option);
                $this.data(DataKey, new Tree($this, options));
            }
        });
    }

    var old = $.fn.tree;

    $.fn.tree             = Plugin;
    $.fn.tree.Constructor = Tree;

    // No Conflict Mode
    // ================
    $.fn.tree.noConflict = function () {
        $.fn.tree = old;
        return this;
    };

    // Tree Data API
    // =============
    $(window).on('load', function () {
        $(Selector.data).each(function () {
            Plugin.call($(this));
        });
    });

}(jQuery);

$(document).ready(function(){
    $('.owl-main-slider').owlCarousel({
        loop: true,
        items: 1,
        margin: 0,
        nav: false,
        dots: false
    });
    var owlSlider = $('.owl-slider');
    owlSlider.owlCarousel({
        loop: true,
        items: 1,
        margin: 0,
        nav: true,
        navText: ['<img src="img/owl-prev.png">', '<img src="img/owl-next.png">'],
        dots: false
    });

    var owlPartners = $('.owl-partners');
    owlPartners.owlCarousel({
        loop: true,
        margin: 10,
        items:6,
        nav: true,
        navText: ['<img src="img/partners-prev.png">', '<img src="img/partners-next.png">'],
        dots: false,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:6
            }
        }
    });

    var owlCertificates = $('.owl-certificates');
    inputTypeSertificates = $("#sertRange");
    owlCertificates.owlCarousel({
        responsive:{
            0:{
                items:1,
                dots: false
            },
            600:{
                items:2,
                dots: false
            },
            1000:{
                items:4,
                dots: false
            }
        }
    });
    owlCertificates.on('changed.owl.carousel', function(event) {
        inputTypeSertificates.val(event.item.index);
    });
    inputTypeSertificates.on("change", function(e) {
        e.preventDefault();
        owlCertificates.trigger('to.owl.carousel', [inputTypeSertificates.val(),1,true]);
    });

    var owlHasDone = $(".owl-hasDone"),
        inputTypeDone = $("#hasDoneRange");
    owlHasDone.owlCarousel({
        'responsive': {
            0: {
                items: 1,
                slideBy: 1,
                dots: false
            },
            600: {
                items: 2,
                slideBy: 1,
                dots: false
            },
            1280: {
                items: 4,
                slideBy: 1,
                dots: false
            }
        }
    });
    owlHasDone.on('changed.owl.carousel', function(event) {
        inputTypeDone.val(event.item.index);
    });
    inputTypeDone.on("change", function(e) {
        e.preventDefault();
        owlHasDone.trigger('to.owl.carousel', [inputTypeDone.val(),1,true]);
    });

    var owl = $(".owl-catalog"),
        inputTypeCatalog = $("#catalogRange");
    owl.owlCarousel({
        'responsive': {
            0: {
                items: 1,
                slideBy: 1,
                URLhashListener: true,
                startPosition: 'URLHash'
            },
            600: {
                items: 2,
                slideBy: 1,
                URLhashListener: true,
                startPosition: 'URLHash'
            },
            1280: {
                items: 4,
                slideBy: 1,
                URLhashListener: true,
                startPosition: 'URLHash'
            }
        }
    });
    owl.on('changed.owl.carousel', function(event) {
        inputTypeCatalog.val(event.item.index);
    });
    inputTypeCatalog.on("change", function(e) {
        e.preventDefault();
        owl.trigger('to.owl.carousel', [inputTypeCatalog.val(),1,true]);
    });

    var owlProject = $(".owl-projects"),
        inputTypeProject = $("#projectRange");
    owlProject.owlCarousel({
        'responsive': {
            0: {
                items: 1,
                slideBy: 1,
                URLhashListener: true,
                startPosition: 'URLHash'
            },
            600: {
                items: 2,
                slideBy: 1,
                URLhashListener: true,
                startPosition: 'URLHash'
            },
            1280: {
                items: 4,
                slideBy: 1,
                URLhashListener: true,
                startPosition: 'URLHash',
                dots: false
            }
        }
    });
    owlProject.on('changed.owl.carousel', function(event) {
        inputTypeProject.val(event.item.index);
    });
    inputTypeProject.on("change", function(e) {
        e.preventDefault();
        owlProject.trigger('to.owl.carousel', [inputTypeProject.val(),1,true]);
    });

    var owlNews = $(".owl-news"),
        inputTypeNews = $("#newsRange");
    owlNews.owlCarousel({
        'responsive': {
            0: {
                items: 1,
                slideBy: 1,
                dots: false
            },
            600: {
                items: 2,
                slideBy: 1,
                dots: false
            },
            1280: {
                items: 3,
                slideBy: 1,
                dots: false
            }
        }
    });
    owlNews.on('changed.owl.carousel', function(event) {
        inputTypeNews.val(event.item.index);
    });
    inputTypeNews.on("change", function(e) {
        e.preventDefault();
        owlNews.trigger('to.owl.carousel', [inputTypeNews.val(),1,true]);
    });

    var owlMontage = $(".owl-montage"),
        inputTypeMontage = $("#montageRange");
    owlMontage.owlCarousel({
        'responsive': {
            0: {
                items: 1,
                slideBy: 1,
                dots: false
            },
            600: {
                items: 2,
                slideBy: 1,
                dots: false
            },
            1280: {
                items: 4,
                slideBy: 1,
                dots: false
            }
        }
    });
    owlMontage.on('changed.owl.carousel', function(event) {
        inputTypeMontage.val(event.item.index);
    });
    inputTypeMontage.on("change", function(e) {
        e.preventDefault();
        owlMontage.trigger('to.owl.carousel', [inputTypeMontage.val(),1,true]);
    });

    var owlReview = $(".owl-review"),
        inputTypeReview = $("#reviewRange");
    owlReview.owlCarousel({
        'responsive': {
            0: {
                items: 1,
                slideBy: 1,
                dots: false
            },
            600: {
                items: 2,
                slideBy: 1,
                dots: false
            },
            1280: {
                items: 4,
                slideBy: 1,
                dots: false
            }
        }
    });
    owlReview.on('changed.owl.carousel', function(event) {
        inputTypeReview.val(event.item.index);
    });
    inputTypeReview.on("change", function(e) {
        e.preventDefault();
        owlReview.trigger('to.owl.carousel', [inputTypeReview.val(),1,true]);
    });

    $('#collapseExample').on('shown.bs.collapse', function () {
        $('.collapseButtonHide').hide();
    });
});

